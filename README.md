Atlassium Scalenium
===================

Atlassium Scalenium is a library that sprinkles a tiny but very handy bit of syntactic sugar on top of
[Atlassian Selenium][atlassian-selenium] and [ScalaTest][scalatest] that will allow you to write reliable
[WebDriver][webdriver] tests with minimal effort.

## Usage

For Maven projects simply add the necessary dependency to your project, then mix in the `TimedAssertions` trait and
start using `shouldEventually` in your test code where you would normally use ScalaTest's `should` or `must`.

### Step 1: Add the Maven dependency

Add the library as test dependency in your Maven project.

    <dependencies>
        ...

        <dependency>
            <groupId>com.atlassian.scalenium</groupId>
            <artifactId>atlassian-scalenium</artifactId>
            <version>1.0</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

### Step 2: assert with shouldEventually

You can now use `shouldEventually` anywhere where you would normally use `should` or `must`. The advantage of doing this
is that `shouldEventually` uses a [TimedQuery][timedquery-docs] under the hood (with a preset timeout) thus making your
test much more resilient to race conditions that occur when testing asynchronous JavaScript applications.

    import junit.framework.TestCase
    import com.atlassian.scalenium.assertion.TimedAssertions

    class MyTestCase extends TestCase with TimedAssertions {
      val product = TestedProductFactory.create(classOf[JiraTestedProduct])

      def testSomething() {
        myPageObject.getAjaxyThing shouldEventually include ("text returned by async call")
      }

Note that `myPageObject.getAjaxyThing()` returns a raw `java.lang.String` in this example, but it will transparently get
promoted to a `TimedQuery<String>` so that if it _eventually_ returns the expected result the assertion will still pass.

  [scalatest]: http://www.scalatest.org/
  [webdriver]: http://docs.seleniumhq.org/projects/webdriver/
  [timedquery-docs]: https://ecosystem.atlassian.net/wiki/x/RQBxAw
  [atlassian-selenium]: https://www.bitbucket.com/atlassian/atlassian-selenium
